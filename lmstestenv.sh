#!/bin/bash

clear

function check_result () {
	if [ $? -ne 0 ] ; then
		echo -e "\nSomething went wrong. Exiting.\\n"
		exit 1
	fi
}

echo -n -e "Enter site name (as appears in config.php): "
read -r site
echo -n -e "Enter Totara major version number [12/13]: " # add check for only 12, 13 etc.
read -r version
echo -n -e "Assuming .gz/.tar.gz backups are in /home/uptmp. Continue? [Y/y] "
read -r answer

if [[ "$answer" != [Yy]* ]] ; then
	echo "Upload backups before running this script. Exiting."
	exit 1
else
	if [ -d "/home/$site" ] ; then
		echo -n -e "Directory exists. Clear contents of /home/$site? [Y/y] "
		read -r answer
			if [[ "$answer" == [Yy]* ]] ; then
			rm -rf /home/"${site:?}"/* >/dev/null
		else
			echo "Remove conflicting directory before running this script. Exiting."
			exit 1
		fi
	fi
fi

function unpack_zip () {
  if [ -f /home/uptmp/public_html.zip ] ; then
    echo -n "Making new directories at /home/$site... "
    mkdir -p /home/$site/{public_html,totara_data} >/dev/null
    check_result
    pushd /home/$site/public_html >/dev/null
    mv /home/uptmp/public_html.zip . >/dev/null
    check_result
    echo "OK"
    echo -n "Unpacking public_html.zip... "
    unzip *.zip >/dev/null
    check_result
    echo "OK"
    echo -n "Removing public_html.zip... "
    rm -f *.zip >/dev/null
    check_result
    echo "OK"
    popd >/dev/null
    pushd /home/$site/totara_data >/dev/null
    mv /home/uptmp/totara_data.zip . >/dev/null
    check_result
    echo -n "Unpacking totara_data.zip... "
    unzip *.zip >/dev/null
    check_result
    echo "OK"
    echo -n "Removing totara_data.zip... "
    rm -f *.zip >/dev/null
    check_result
    echo "OK"
    popd >/dev/null
  fi
}

function unpack_tgz () {
  if [ -f /home/uptmp/public_html.tar.gz ] ; then
    echo -n "Making new directories at /home/$site... "
    mkdir -p /home/$site/{public_html,totara_data} >/dev/null
    check_result
    pushd /home/$site/public_html >/dev/null
    mv /home/uptmp/public_html.tar.gz . >/dev/null
    check_result
    echo "OK"
    echo -n "Unpacking public_html.tar.gz... "
    tar -xf *.tar.gz >/dev/null
    check_result
    echo "OK"
    echo -n "Removing public_html.tar.gz... "
    rm -f *.tar.gz >/dev/null
    check_result
    echo "OK"
    popd >/dev/null
    pushd /home/$site/totara_data >/dev/null
    mv /home/uptmp/totara_data.tar.gz . >/dev/null
    check_result
    echo -n "Unpacking totara_data.tar.gz... "
    tar -xf *.tar.gz >/dev/null
    check_result
    echo "OK"
    echo -n "Removing totara_data.tar.gz... "
    rm -f *.tar.gz >/dev/null
    check_result
    echo "OK"
    popd >/dev/null
  fi
}

function unpack_home_tgz () {
  if [ -f /home/uptmp/backup*.tar.gz ] ; then
    mkdir /home/uptmp/home-backup >/dev/null
    check_result
    pushd /home/uptmp/home-backup >/dev/null
    home=$(ls /home/uptmp/backup*.tar.gz)
    mv $home . >/dev/null
    check_result
    echo -n "Unpacking /home/uptmp/backup*.tar.gz... "
    tar -xf *.tar.gz >/dev/null
    check_result
    echo "OK"
    echo -n "Removing /home/uptmp/backup*.tar.gz... "
    rm -f *.tar.gz >/dev/null
    check_result
    echo "OK"
    echo -n "Making new directory at /home/$site... "
    mkdir /home/$site
    check_result
    echo "OK"
    echo -n "Moving public_html and totara_data... "
    mv public_html totara_data -t /home/$site/
    check_result
    echo "OK"
    popd >/dev/null
  fi
}

echo "Setting up test environment for $site."
if [ -f /home/uptmp/public_html.zip ] ; then
  archive="zip"
elif [ -f /home/uptmp/public_html.tar.gz ] ; then
  archive="tgz"
elif [ -f /home/uptmp/backup*.tar.gz ] ; then
  archive="home_tgz"
else
  echo "Upload correct backups before running this script. Exiting."
fi

case $archive in
  zip)
    unpack_zip ;;
  tgz)
    unpack_tgz ;;
  home_tgz)
    unpack_home_tgz ;;
esac

if [ -f /home/$site/public_html/.htaccess ] ; then
	echo -n "Disabling .htaccess... "
	mv /home/$site/public_html/.htaccess /home/$site/public_html/..htaccess
	check_result
	echo "OK"
fi

echo -n "Updating httpd configs... "
if [ $version -eq "13" ] ; then
	sed -i "s/placeholder\/public_html/$site\/public_html\/server/g" /etc/httpd/conf.d/vhosts.conf /etc/httpd/conf/httpd.conf >/dev/null
	check_result
else
	sed -i "s/placeholder/$site/g" /etc/httpd/conf.d/vhosts.conf /etc/httpd/conf/httpd.conf >/dev/null
	check_result
fi
echo "OK"

echo -n "Setting up config.php... "
chmod +w /home/$site/public_html/config.php >/dev/null
check_result
sed -i "s/.*dbname.*/\$CFG->dbname = 'alma_totara';/g" /home/$site/public_html/config.php >/dev/null
check_result
sed -i "s/.*dbuser.*/\$CFG->dbuser = 'alma_totara';/g" /home/$site/public_html/config.php >/dev/null
check_result
sed -i "s/.*dbpass.*/\$CFG->dbpass = 'Alma123!';/g" /home/$site/public_html/config.php >/dev/null
check_result
sed -i "s/.*noemailever.*/\$CFG->noemailever = true;/g" /home/$site/public_html/config.php >/dev/null
check_result
echo "\$CFG->noemailever = true;" >> /home/$site/public_html/config.php
check_result
sed -i "s/.*wwwroot.*/\$CFG->wwwroot = 'https:\/\/alma';/g" /home/$site/public_html/config.php >/dev/null
check_result
echo "OK"

echo -n "Unpacking database... "
pushd /home/uptmp >/dev/null
gunzip ./*.sql.gz >/dev/null
check_result
popd >/dev/null
echo "OK"

echo -n "Importing database... "
mysql alma_totara < /home/uptmp/*.sql >/dev/null
check_result
echo "OK"

echo -n "Giving apache ownership of public_html... "
chown -R apache:apache /home/$site/public_html >/dev/null
check_result
echo "OK"

echo -n "Restarting httpd service... "
systemctl restart httpd.service >/dev/null
check_result
echo "OK"

exit 0
