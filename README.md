# lmstestenv.sh

This script will quickly create/move/modify directories and files, and import the relevant database, to set up an environment for Totara LMS testing (v12>) of single sites. The app and app data will be taken from backup archives in `/home/uptmp` and put in place under `/home/<site>`.

Note that this script was written with the intention to be used on a dedicated AlmaLinux VM, used only for offline/sandboxed testing.
